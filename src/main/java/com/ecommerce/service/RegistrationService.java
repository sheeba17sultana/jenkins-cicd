package com.ecommerce.service;

import java.util.List;

import com.ecommerce.model.Registration;



public interface RegistrationService {
	
	public Registration submitRegistration(Registration registration);
	public List<Registration> getAllRegisteredUsers();
	public Registration getRegisteredUserByPhone(long phone);

}
